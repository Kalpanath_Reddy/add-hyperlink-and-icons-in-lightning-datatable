import { LightningElement, wire, api, track } from 'lwc';
import fetchCases from '@salesforce/apex/CaseLWCService.fetchCases';

const columns = [
    { label: 'CaseNumber', fieldName: 'CaseNumber' },
    { label: 'Subject', fieldName: 'Subject', wrapText: true},
    { label: 'Status', fieldName: 'Status' },
    { label: 'Priority', fieldName: 'Priority' },
    { label: 'Contact', fieldName: 'ContactName', wrapText: true },
    { label: 'Account', fieldName: 'AccountName', wrapText: true }
];
export default class CaseDatatable extends LightningElement {
    
    @api result;
    @track error;

    columnsList = columns;
    
    connectedCallback(){
        this.getAllCaseDetails();
    }

    getAllCaseDetails(){
        fetchCases()
            .then(data => {
                /* Iterate with Each record and check if the Case is Associated with Account or Contact
                    then get the Name and display into datatable
                */
                data.forEach(caseRec => {
                    if(caseRec.ContactId){
                        caseRec.ContactName = caseRec.Contact.Name;
                    }
                    if(caseRec.AccountId){
                        caseRec.AccountName = caseRec.Account.Name;
                    }
                });
                this.result = data;
                window.console.log(' data ', data);
                this.error = undefined;
            })
            .catch(error => {
                this.error = error;
                window.console.log(' error ', error);
                this.result = undefined;
            });
    }
    
    handleRowAction(){
        
    }
}